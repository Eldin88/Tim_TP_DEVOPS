# Application d'exemple pour le travail pratique sur le Cloud

## Installation

```bash
git clone <URL du dépôt>
cd example-app
npm install # Installation des dépendances
```

## Lancement

```bash
npm start
```

## Utilisation de Docker dans un pipeline GitLab CI

Pour utiliser Docker dans un pipeline, ajouter ces deux lignes en haut du fichier `.gitlab-ci.yml`:

```yaml
services:
  - docker:dind
```
